# Generation of Python API

```
mkdir -p build
cd build
cmake ..
make 
```

# Usage (from build directory)

```
export PYTHONPATH=.
python
import example
example.fact(5)
```
